﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hospital.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using hospital.Repositories;
using hospital.Interfaces;
using hospital.Services;
using AutoMapper;
using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using hospital.Helpers;

namespace hospital
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            // enable portable dev database
            _contentRootPath = env.ContentRootPath;
        }

        public IConfiguration Configuration { get; }
        private string _contentRootPath = "";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            .AddJsonOptions(
            options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            string conn = Configuration.GetConnectionString("DefaultConnection");
            if (conn.Contains("%CONTENTROOTPATH%"))
            {
                conn = conn.Replace("%CONTENTROOTPATH%", _contentRootPath);
            }
            services.AddDbContext<DataContext>(options => 
                options.UseSqlServer(conn));

            services.AddScoped<IPatientRepo, PatientRepo>();
            services.AddScoped<IPatientService, PatientService>();
            services.AddScoped<IHospitalRepo, HospitalRepo>();
            services.AddScoped<IHospitalService, HospitalService>();
            services.AddScoped<IIllnessRepo, IllnessRepo>();
            services.AddScoped<IIllnessService, IllnessService>();
            services.AddScoped(typeof(IBaseRepo<>), typeof(BaseRepo<>));

            services.AddAutoMapper(typeof(Startup));
            services.AddCors();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // global exception handler
                app.UseExceptionHandler(builder =>
                {
                    builder.Run(async context => {
                        // TODO: Implement custom exception handler for status code
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if (error != null)
                        {
                            // add error to header of respond
                            context.Response.AddApplicationError(error.Error.Message);
                            // add error to body of respond
                            await context.Response.WriteAsync(error.Error.Message);
                        }
                    });
                });
                app.UseHsts();
            }

            app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
