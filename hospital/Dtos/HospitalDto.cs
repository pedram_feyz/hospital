using hospital.Models;

namespace hospital.Dtos
{
    public class HospitalDto: BaseModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int AvgProcessTimePerPerson { get; set; }
        public int NumberOfPatiens { get; set; }
        public int WaitingTime { get; set; }
    }
}