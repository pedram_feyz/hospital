using hospital.Models;

namespace hospital.Dtos
{
    public class PatientDto
    {
        public string FirstName { get; set; }
        public string LasttName { get; set; }
        public int Age { get; set; }
        public int Severity { get; set; }
        public int HospitalId { get; set; }
        public Hospital Hospital{ get; set; }
        public int IllnessId { get; set; }
        public Illness Illness { get; set; }
    }
}