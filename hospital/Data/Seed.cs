using System.Collections.Generic;
using hospital.Models;
using Microsoft.EntityFrameworkCore;

namespace hospital.Data
{
    public static class Seed
    {
        public static void Seeding(this ModelBuilder modelBuilder){
            var hospitals = new List<Hospital>()
            {
                new Hospital() { ID = 1, Name = "Balmain", Address = "29 Booth St, Balmain NSW 2041", AvgProcessTimePerPerson = 25},
                new Hospital() { ID = 2, Name = "Concord", Address = "Hospital Rd, Concord NSW 2137", AvgProcessTimePerPerson = 30},
                new Hospital() { ID = 3, Name = "Weastmead", Address = "Hawkesbury Rd, Westmead NSW 2145", AvgProcessTimePerPerson = 35},
                new Hospital() { ID = 4, Name = "RPA", Address = "50 Missenden Rd, Camperdown NSW 2050", AvgProcessTimePerPerson = 40},
                new Hospital() { ID = 5, Name = "Canterbury", Address = "575 Canterbury Rd, Campsie NSW 2194", AvgProcessTimePerPerson = 45},
            };
            modelBuilder.Entity<Hospital>().HasData(hospitals.ToArray());

            var illnesses = new List<Illness>()
            {
                new Illness(){ ID = 1, Name = "Mortal Cold"},
                new Illness(){ ID = 2, Name = "Happy Euphoria"},
                new Illness(){ ID = 3, Name = "Withering Parasite"},
                new Illness(){ ID = 4, Name = "Spirit Parasite"},
                new Illness(){ ID = 5, Name = "Death's Delusions"},
                new Illness(){ ID = 6, Name = "Crippiling Paranoia"},
                new Illness(){ ID = 7, Name = "Impossible Ebola"},
            };
            modelBuilder.Entity<Illness>().HasData(illnesses.ToArray());

            var patientes = new List<Patient>()
            {
                new Patient(){ID = 1, FirstName = "James", LasttName = "Bound", Age = 40, IllnessId = 1, HospitalId = 1, Severity = 1},
                new Patient(){ID = 2, FirstName = "Dave", LasttName = "Williams", Age = 20, IllnessId = 2, HospitalId = 1, Severity = 1},
                new Patient(){ID = 3, FirstName = "Debra", LasttName = "Robinson", Age = 34, IllnessId = 1, HospitalId = 2, Severity = 2},
                new Patient(){ID = 4, FirstName = "Melody", LasttName = "Cooper", Age = 19, IllnessId = 3, HospitalId = 3, Severity = 3},
            };
            modelBuilder.Entity<Patient>().HasData(patientes.ToArray());
        }
    }
}