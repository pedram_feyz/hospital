using hospital.Models;
using Microsoft.EntityFrameworkCore;

namespace hospital.Data
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options): base(options)
        {
            
        }
        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<Patient> Patientes { get; set; }
        public DbSet<Illness> Illnesses { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seeding();
        }
    }
}