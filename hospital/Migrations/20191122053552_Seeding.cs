﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace hospital.Migrations
{
    public partial class Seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Hospitals",
                columns: new[] { "ID", "Address", "AvgProcessTimePerPerson", "Name" },
                values: new object[,]
                {
                    { 1, "29 Booth St, Balmain NSW 2041", 25, "Balmain" },
                    { 2, "Hospital Rd, Concord NSW 2137", 30, "Concord" },
                    { 3, "Hawkesbury Rd, Westmead NSW 2145", 35, "Weastmead" },
                    { 4, "50 Missenden Rd, Camperdown NSW 2050", 40, "RPA" },
                    { 5, "575 Canterbury Rd, Campsie NSW 2194", 45, "Canterbury" }
                });

            migrationBuilder.InsertData(
                table: "Illnesses",
                columns: new[] { "ID", "Name" },
                values: new object[,]
                {
                    { 1, "Mortal Cold" },
                    { 2, "Happy Euphoria" },
                    { 3, "Withering Parasite" },
                    { 4, "Spirit Parasite" },
                    { 5, "Death's Delusions" },
                    { 6, "Crippiling Paranoia" },
                    { 7, "Impossible Ebola" }
                });

            migrationBuilder.InsertData(
                table: "Patientes",
                columns: new[] { "ID", "Age", "FirstName", "HospitalId", "IllnessId", "LasttName", "Severity" },
                values: new object[,]
                {
                    { 1, 40, "James", 1, 1, "Bound", 1 },
                    { 3, 34, "Debra", 2, 1, "Robinson", 2 },
                    { 2, 20, "Dave", 1, 2, "Williams", 1 },
                    { 4, 19, "Melody", 3, 3, "Cooper", 3 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Hospitals",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Hospitals",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Illnesses",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Illnesses",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Illnesses",
                keyColumn: "ID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Illnesses",
                keyColumn: "ID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Patientes",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Patientes",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Patientes",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Patientes",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Hospitals",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Hospitals",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Hospitals",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Illnesses",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Illnesses",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Illnesses",
                keyColumn: "ID",
                keyValue: 3);
        }
    }
}
