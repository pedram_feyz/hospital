using Microsoft.AspNetCore.Http;

namespace hospital.Helpers
{
    public static class Extensions
    {
        public static void AddApplicationError(this HttpResponse response, string messsage)
        {
            response.Headers.Add("Application-Error", messsage);
            //response.Headers.Add("Access-Control-Expose-Header", "Application-Error");
        }
    }
}