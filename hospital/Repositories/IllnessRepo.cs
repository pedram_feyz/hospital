using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Data;
using hospital.Interfaces;
using hospital.Models;
using Microsoft.EntityFrameworkCore;

namespace hospital.Repositories
{
    public class IllnessRepo: BaseRepo<Illness>, IIllnessRepo
    {
        public IllnessRepo(DataContext context) :base(context)
        {

        }

        public override async Task<Illness> Get(int id)
        {
            var illness = await base.Get(id);
            return illness;
        }

        public new async Task<List<Illness>> GetAll()
        {
            //TODO add pagination
            var queryIllness = base.GetAll();
            var illnesses = await queryIllness.ToListAsync();
            return illnesses;
        }
        public async override Task<Illness> Create(Illness illness)
        {
            var createdIllness = await base.Create(illness);
            return createdIllness;
        }

        public async override Task<bool> Delete(Illness illness)
        {
            return await base.Delete(illness);
        }

        public new async Task<bool> SaveAll()
        {
            return await base.SaveAll();
        }
    }
}