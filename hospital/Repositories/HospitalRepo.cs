using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using hospital.Data;
using hospital.Dtos;
using hospital.Interfaces;
using hospital.Models;
using Microsoft.EntityFrameworkCore;

namespace hospital.Repositories
{
    public class HospitalRepo: BaseRepo<Hospital>, IHospitalRepo
    {
        private readonly IMapper _mapper;
        public HospitalRepo(DataContext context, IMapper mapper) :base(context)
        {
            _mapper = mapper;
        }

        public async Task<HospitalDto> GetWithPatientCounting(int id)
        {
            var hospitalQuery = base.GetQuery();
            var hospitalDto = await hospitalQuery.Select(hos =>
                    new HospitalDto()
                        {
                            ID = hos.ID,
                            Name = hos.Name,
                            Address = hos.Address,
                            AvgProcessTimePerPerson = hos.AvgProcessTimePerPerson,
                            NumberOfPatiens = hos.Patients.Count(),
                            // this one should be done in mapping
                            WaitingTime = hos.AvgProcessTimePerPerson * hos.Patients.Count()
                        }
                ).Where(h => h.ID == id).OrderBy(x => x.WaitingTime).FirstOrDefaultAsync(); 
            return hospitalDto;
        }

        public new async Task<Hospital> Get(int id)
        {
            var hospitalQuery = base.GetQuery();
            return await hospitalQuery.FirstOrDefaultAsync(h => h.ID == id);
        }
        public new async Task<List<HospitalDto>> GetAll()
        {
            //TODO add pagination
            var hospitalQuery = base.GetAll();
            var hospitalsDto = await hospitalQuery.Select(hos =>
                    new HospitalDto()
                        {
                            ID = hos.ID,
                            Name = hos.Name,
                            Address = hos.Address,
                            AvgProcessTimePerPerson = hos.AvgProcessTimePerPerson,
                            NumberOfPatiens = hos.Patients.Count(),
                            WaitingTime = hos.AvgProcessTimePerPerson * hos.Patients.Count()
                        }
                ).OrderBy(x => x.WaitingTime).ToListAsync(); 

            return hospitalsDto;
        }

        public async Task<List<HospitalDto>> GetAllByOrder(int id)
        {
            //TODO add pagination
            var hospitalQuery = base.GetAll();
            var hospitalsDto = await hospitalQuery.Select(hos =>
                    new HospitalDto()
                        {
                            ID = hos.ID,
                            Name = hos.Name,
                            Address = hos.Address,
                            AvgProcessTimePerPerson = hos.AvgProcessTimePerPerson,
                            NumberOfPatiens = hos.Patients.Where(p => p.Severity >= id).Count(),
                            WaitingTime = hos.AvgProcessTimePerPerson * hos.Patients.Where(p => p.Severity >= id).Count()
                        }
                ).OrderBy(x => x.Name).OrderBy(x => x.WaitingTime).ToListAsync(); 

            return hospitalsDto;
        }

        public new async Task<Hospital> Create(Hospital hospital)
        {
            var createdHospital = await base.Create(hospital);
            return createdHospital;
        }

        public new async Task<bool> Delete(Hospital hospital)
        {
            return await base.Delete(hospital);
        }

        public new async Task<bool> SaveAll()
        {
            return await base.SaveAll();
        }
    }
}