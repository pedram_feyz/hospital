using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Data;
using hospital.Interfaces;
using hospital.Models;
using Microsoft.EntityFrameworkCore;

namespace hospital.Repositories
{
    public class PatientRepo: BaseRepo<Patient>, IPatientRepo
    {
        public PatientRepo(DataContext context) :base(context)
        {

        }

        public override async Task<Patient> Get(int id)
        {
            var patient = await base.Get(id);
            return patient;
        }

        public async Task<Patient> GetInclude(int id)
        {
            var patientQuery =  base.GetQuery();
            var patient = await patientQuery
                    .Include(p => p.Hospital).
                    Include(p => p.Illness)
                    .FirstAsync(p => p.ID == id);
            return patient;
        }

        public new async Task<List<Patient>> GetAll()
        {
            //TODO add pagination
            var queryPatients = base.GetAll();
            var patients = await queryPatients.ToListAsync();
            return patients;
        }

        public async Task<List<Patient>> GetAllInclude()
        {
            //TODO add pagination
            var queryPatients = base.GetAll();
            var patients = await queryPatients
                    .Include(p => p.Hospital)
                    .Include(p => p.Illness)
                    .ToListAsync();
            return patients;
        }

        public new async Task<Patient> Create(Patient patient)
        {
            var createdPatient = await base.Create(patient);
            return createdPatient;
        }

        public new async Task<bool> Delete(Patient patient)
        {
            return await base.Delete(patient);
        }

        public new async Task<bool> SaveAll()
        {
            return await base.SaveAll();
        }
    }
}