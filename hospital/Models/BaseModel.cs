using hospital.Interfaces;

namespace hospital.Models
{
    public class BaseModel: IEntity
    {
        public int ID { get ; set; }
    }
}