using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace hospital.Models
{
    public class Illness: BaseModel
    {
        [Required]
        public string Name { get; set; }
        public IEnumerable<Patient> Patients { get; set; }
    }
}