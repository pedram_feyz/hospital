using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace hospital.Models
{
    public class Hospital: BaseModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        public int AvgProcessTimePerPerson { get; set; }
        public IEnumerable<Patient> Patients { get; set; }
    }
}