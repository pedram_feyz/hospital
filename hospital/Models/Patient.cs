using System.ComponentModel.DataAnnotations;

namespace hospital.Models
{
    public class Patient: BaseModel
    {
        [Display(Name="First Name")]
        public string FirstName { get; set; }
        public string LasttName { get; set; }
        public int Age { get; set; }
        [Range(0, 4)]
        public int Severity { get; set; }
        public int HospitalId { get; set; }
        public Hospital Hospital{ get; set; }
        public int IllnessId { get; set; }
        public Illness Illness { get; set; }
    }
}