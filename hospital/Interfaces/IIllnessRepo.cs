using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Models;

namespace hospital.Interfaces
{
    public interface IIllnessRepo
    {
        Task<Illness> Get(int id);
        Task<List<Illness>> GetAll();
        Task<Illness> Create(Illness illness);
        Task<bool> Delete(Illness illness);
        Task<bool> SaveAll();
    }
}