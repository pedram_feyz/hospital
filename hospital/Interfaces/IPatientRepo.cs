using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Models;

namespace hospital.Interfaces
{
    public interface IPatientRepo
    {
        Task<Patient> Get(int id);
        Task<Patient> GetInclude(int id);
        Task<List<Patient>> GetAll();
        Task<List<Patient>> GetAllInclude();
        Task<Patient> Create(Patient patient);
        Task<bool> Delete(Patient patient);
        Task<bool> SaveAll();
    }
}