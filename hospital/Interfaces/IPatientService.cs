using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Models;

namespace hospital.Interfaces
{
    // TODO use Dto
    public interface IPatientService
    {
        Task<Patient> Get(int id);
        Task<Patient> GetInclude(int id);
        Task<List<Patient>> GetAll();
        Task<List<Patient>> GetAllInclude();
        Task<Patient> Create(Patient patient);
        Task<Patient> Update(int id, Patient patient);
        Task<bool> Delete(int id);
    }
}