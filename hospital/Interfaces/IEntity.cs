namespace hospital.Interfaces
{
    public interface IEntity
    {
         int ID { get; set; }
    }
}