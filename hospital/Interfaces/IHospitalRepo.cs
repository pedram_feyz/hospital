using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Dtos;
using hospital.Models;

namespace hospital.Interfaces
{
    public interface IHospitalRepo
    {
        Task<Hospital> Get(int id);
        Task<HospitalDto> GetWithPatientCounting(int id);
        Task<List<HospitalDto>> GetAll();
        Task<List<HospitalDto>> GetAllByOrder(int id);
        Task<Hospital> Create(Hospital hospital);
        Task<bool> Delete(Hospital hospital);
        Task<bool> SaveAll();
    }
}