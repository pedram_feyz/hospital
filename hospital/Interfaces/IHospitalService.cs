using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Dtos;
using hospital.Models;

namespace hospital.Interfaces
{
    public interface IHospitalService
    {
        Task<HospitalDto> Get(int id);
        Task<List<HospitalDto>> GetAll();
        Task<List<HospitalDto>> GetAllByOrder(int id);
        Task<Hospital> Create(Hospital patient);
        Task<HospitalDto> Update(int id, Hospital patient);
        Task<bool> Delete(int id);
    }
}