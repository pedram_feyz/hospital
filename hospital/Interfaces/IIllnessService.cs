using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Models;

namespace hospital.Interfaces
{
    public interface IIllnessService
    {
        Task<Illness> Get(int id);
        Task<List<Illness>> GetAll();
        Task<Illness> Update(int id, Illness illness);
        Task<Illness> Create(Illness illness);
        Task<bool> Delete(int id);
        Task<bool> SaveAll();
    }
}