using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Interfaces;
using hospital.Models;

namespace hospital.Services
{
    public class IllnessService: IIllnessService
    {
        private readonly IIllnessRepo _illnessRepo;
        public IllnessService(IIllnessRepo illnessRepo)
        {
            _illnessRepo = illnessRepo;

        }

        public async Task<Illness> Create(Illness illness)
        {
            var createdIllness =  await _illnessRepo.Create(illness);
            if (createdIllness == null)
            {
                throw new Exception("Faild to create the Illness.");
            }
            return createdIllness;
        }

        public async Task<bool> Delete(int id)
        {
            var illness = await _illnessRepo.Get(id);
            if (illness == null)
            {
                throw new Exception($"Could not find Illness with an Id: {id}");
            }

            if (await _illnessRepo.Delete(illness)) {
                return true;
            }
            throw new Exception($"Could not delete Illness with an Id: {id}");
        }

        public async Task<Illness> Get(int id)
        {
            var illness = await _illnessRepo.Get(id);
            if (illness == null)
            {
                throw new Exception($"Could not find Illness with an Id: {id}");
            }
            return illness;
        }

        public async Task<List<Illness>> GetAll()
        {
            var illnesss = await _illnessRepo.GetAll();

            return illnesss;
        }

        public Task<bool> SaveAll()
        {
            throw new NotImplementedException();
        }

        public async Task<Illness> Update(int id, Illness illness)
        {
            var entity = await _illnessRepo.Get(id);
            if (entity == null)
            {
                throw new Exception($"Could not find Illness with an Id: {id}");
            }
            
            // TODO use Automapping
            entity.Name = illness.Name;

            if (!await _illnessRepo.SaveAll())
            {
                throw new Exception($"Could not update Illness with an Id: {id}");
            }

            return illness;
        }
    }
}