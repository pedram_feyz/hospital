using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Dtos;
using hospital.Interfaces;
using hospital.Models;

namespace hospital.Services
{
    public class HospitalService: IHospitalService
    {
        private readonly IHospitalRepo _hospitalRepo;
        public HospitalService(IHospitalRepo hospitalRepo)
        {
            _hospitalRepo = hospitalRepo;

        }

        // TODO should return Dto
        public async Task<Hospital> Create(Hospital hospital)
        {
            var createdHospital =  await _hospitalRepo.Create(hospital);
            if (createdHospital == null)
            {
                throw new Exception("Faild to create the hospital.");
            }
            return createdHospital;
        }

        public async Task<bool> Delete(int id)
        {
            var hospital = await _hospitalRepo.Get(id);
            if (hospital == null)
            {
                throw new Exception($"Could not find hospital with an Id: {id}");
            }

            if (await _hospitalRepo.Delete(hospital)) {
                return true;
            }
            throw new Exception($"Could not delete patient with an Id: {id}");
        }


        public async Task<HospitalDto> Get(int id)
        {
            var hospital = await _hospitalRepo.GetWithPatientCounting(id);
            if (hospital == null)
            {
                throw new Exception($"Could not find hospital with an Id: {id}");
            }
            return hospital;
        }

        public async Task<List<HospitalDto>> GetAll()
        {
            var patients = await _hospitalRepo.GetAll();

            return patients;
        }

        public async Task<List<HospitalDto>> GetAllByOrder(int id)
        {
            var patients = await _hospitalRepo.GetAllByOrder(id);

            return patients;
        }

        public async Task<HospitalDto> Update(int id, Hospital hospital)
        {
            var entity = await _hospitalRepo.GetWithPatientCounting(id);
            if (entity == null)
            {
                throw new Exception($"Could not find patient with an Id: {id}");
            }
            
            // TODO use Automapping
            // TODO mapping should be apply otherwise changed does not save

            if (!await _hospitalRepo.SaveAll())
            {
                throw new Exception($"Could not update patient with an Id: {id}");
            }

            return entity;
        }
    }
}