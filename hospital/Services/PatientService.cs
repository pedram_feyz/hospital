using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using hospital.Interfaces;
using hospital.Models;

namespace hospital.Services
{
    public class PatientService: IPatientService
    {
        private readonly IPatientRepo _patientRepo;
        public PatientService(IPatientRepo patientRepo)
        {
            _patientRepo = patientRepo;

        }

        public async Task<Patient> Create(Patient patient)
        {
            var createdPatient =  await _patientRepo.Create(patient);
            if (createdPatient == null)
            {
                throw new Exception("Faild to create the patient.");
            }
            return createdPatient;
        }

        public async Task<bool> Delete(int id)
        {
            var patient = await _patientRepo.Get(id);
            if (patient == null)
            {
                throw new Exception($"Could not find Patient with an Id: {id}");
            }

            if (await _patientRepo.Delete(patient)) {
                return true;
            }
            throw new Exception($"Could not delete patient with an Id: {id}");
        }

        public async Task<Patient> GetInclude(int id)
        {
            var patient = await _patientRepo.GetInclude(id);
            if (patient == null)
            {
                throw new Exception($"Could not find patient with an Id: {id}");
            }
            return patient;
        }

        public async Task<Patient> Get(int id)
        {
            var patient = await _patientRepo.Get(id);
            if (patient == null)
            {
                throw new Exception($"Could not find patient with an Id: {id}");
            }
            return patient;
        }

        public async Task<List<Patient>> GetAll()
        {
            var patients = await _patientRepo.GetAll();

            return patients;
        }

        public async Task<List<Patient>> GetAllInclude()
        {
            var patients = await _patientRepo.GetAllInclude();

            return patients;
        }

        public async Task<Patient> Update(int id, Patient patient)
        {
            var entity = await _patientRepo.Get(id);
            if (entity == null)
            {
                throw new Exception($"Could not find patient with an Id: {id}");
            }
            
            // TODO use Automapping
            entity.FirstName = patient.FirstName;
            entity.LasttName = patient.FirstName;
            entity.Age = patient.Age;
            entity.HospitalId = patient.HospitalId;
            entity.IllnessId = patient.IllnessId;
            entity.Severity = patient.Severity;

            if (!await _patientRepo.SaveAll())
            {
                throw new Exception($"Could not update patient with an Id: {id}");
            }

            return patient;
        }
    }
}