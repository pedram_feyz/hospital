using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hospital.Data;
using hospital.Interfaces;
using hospital.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace hospital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientsController: ControllerBase
    {
        private readonly IPatientService _patientService;
        public PatientsController(IPatientService patientService)
        {
            _patientService = patientService;
        }
        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var addresses = await _patientService.GetAllInclude();
            return Ok(addresses);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var patient = await _patientService.GetInclude(id);
            return Ok(patient);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post(Patient patient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var createdPatient = await _patientService.Create(patient);
            return Ok(createdPatient);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, Patient patient) 
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var UpdatedPatient = await _patientService.Update(id, patient);
            return Ok(UpdatedPatient);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _patientService.Delete(id));
        }
    }
}