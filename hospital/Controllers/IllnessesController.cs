using System.Threading.Tasks;
using hospital.Interfaces;
using hospital.Models;
using Microsoft.AspNetCore.Mvc;

namespace hospital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IllnessesController: ControllerBase
    {
        private readonly IIllnessService _illnessService;
        public IllnessesController(IIllnessService illnessService)
        {
            _illnessService = illnessService;
        }
        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var illnesses = await _illnessService.GetAll();
            return Ok(illnesses);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var illness = await _illnessService.Get(id);
            return Ok(illness);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post(Illness illness)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var createdIllness = await _illnessService.Create(illness);
            return Ok(createdIllness);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, Illness illness) 
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var UpdatedIllness = await _illnessService.Update(id, illness);
            return Ok(UpdatedIllness);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _illnessService.Delete(id));
        }
    }
}