using System.Threading.Tasks;
using hospital.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hospital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HospitalsController: ControllerBase
    {
        private readonly IHospitalService _hospitalService;
        public HospitalsController(IHospitalService hospitalService)
        {
            _hospitalService = hospitalService;
        }
        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var hospitals = await _hospitalService.GetAll();
            return Ok(hospitals);
        }

        [HttpGet("severity/{id}")]
        public async Task<IActionResult> GetAllByOrder(int id)
        {
            var hospital = await _hospitalService.GetAllByOrder(id);
            return Ok(hospital);
        }
        

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var hospital = await _hospitalService.Get(id);
            return Ok(hospital);
        }
    }
}