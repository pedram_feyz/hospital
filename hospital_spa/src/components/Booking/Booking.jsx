import React, { PureComponent } from 'react';
import { booking } from '../../services/http'
import  './Booking.scss';

class Booking extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      patientName: '',
      age: ''
    };
  }

submit = () => {
  const values = {
    FirstName: this.state.patientName,
    Age: this.state.age || 0,
    Severity: this.props.severity.value,
    HospitalId: this.props.hospital.value,
    IllnessId: this.props.illness.value
  };
  booking("Patients", values, this.callBack);
}

callBack = (data) =>{
  alert("your request has been booked.");
  console.log("create");
  console.log(data);
  this.setState({patientName: ''});
  this.setState({age: ''});
  this.props.onHandleChange({"name":"illness","value":{ "name":'', "value": 1}});
  this.props.onHandleChange({"name":"severity","value":{ "name":'', "value": -1}});
  this.props.onHandleChange({"name":"hospital","value":{ "name":'', "value": 1}});
};

updateInputValue= (evt) => {
  this.setState({
    [evt.target.name]: evt.target.value
  });
}

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div className="BookingWrapper">
        <ul>
          <li>
            <input type="text" placeholder="Patient name" name="patientName" value={this.state.patientName} onChange={this.updateInputValue} />
          </li>
          <li>
            <input type="text" placeholder="Age" name="age" value={this.state.age} onChange={this.updateInputValue} />
          </li>
          <li>
            <input type="text" placeholder="Illness" value={this.props.illness.name} readOnly />
          </li>
          <li>
            <input type="text" placeholder="Hospital" value={this.props.hospital.name} readOnly />
          </li>
          <li>
            <input type="text" placeholder="Severity" value={this.props.severity.name} readOnly />
          </li>
          <li>
            <input type="button" value="Submit" onClick={() => this.submit()} />
          </li>
        </ul>
      </div>
    );
  }
}

export default Booking;
