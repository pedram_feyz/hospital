import React, { PureComponent } from 'react';
import HospitalCard from './HospitalCard';
import { getAll } from '../../services/http';
import './Hospitals.scss';

class Hospitals extends PureComponent { 
  constructor(props) {
    super(props);

    this.gridItems  = [];
    this.state = {
      hospitals: {},
      isLoading: true,
      hasError: false,
    };
  }

  handleChange = (obj) => {
    this.props.onHandleChange(obj);
  }

  callBack = (data) => {
    this.setState({ hospitals: data}, () => {
      this.gridItems  = this.state.hospitals.map((hos) => {
        return HospitalCard(hos, this.handleChange);
      });
    });
    this.setState({isLoading: false});
  }

  componentDidMount() {
    getAll("hospitals/severity/" + this.props.severity, this.callBack);
  }  
  componentDidUpdate(prevProps){
    if(this.props.severity != prevProps.severity)
    {
      let severity =  this.props.severity;
      if(severity < 0 || severity > 4){
        severity = 0;
      }
      this.setState({isLoading: true});
      getAll("hospitals/severity/" + severity, this.callBack);
    }
    
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    if (this.gridItems.length == 0) {
      return <h1>is loading ...</h1>;
    }
    return (
      <div className="HospitalsWrapper">
        <h4>Our suggested hospitals:</h4>
        <ul>
          {this.gridItems}
        </ul>
      </div>
    );
  }
}

export default Hospitals;
