import React from 'react';
import './HospitalCard.scss';

const HospitalCard = (props, change) => (
  <li tabIndex={0} key={props.id} onClick={() => change({"name":"hospital","value":{ "name":props.name, "value": props.id}})} className="HospitalCardWrapper">
    <span>{props.name}</span>
    <span> wait time:</span>
    <span>{props.waitingTime < 60 ? props.waitingTime : ((Math.floor(props.waitingTime/60))+ ":"+(props.waitingTime % 60) )}</span>
    <span>{props.waitingTime < 60 ? "mins" : "hrs"}</span>
    <br />
    <span> Number Of patient in waiting list with same pain severity or more:</span>
    <span>{props.numberOfPatiens}</span>
    <br />
    <span>Avg Process Time Per Person: </span>
    <span>{props.avgProcessTimePerPerson < 60 ? props.avgProcessTimePerPerson : ((Math.floor(props.avgProcessTimePerPerson/60))+ ":"+(props.avgProcessTimePerPerson % 60) )}</span>
    <span>{props.waitingTime < 60 ? "mins" : "hrs"}</span>
  </li>
);

export default HospitalCard;
