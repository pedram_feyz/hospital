import React from 'react';
import painLevel0 from '../../image/painLevel0.JPG';
import painLevel1 from '../../image/painLevel1.JPG';
import painLevel2 from '../../image/painLevel2.JPG';
import painLevel3 from '../../image/painLevel3.JPG';
import painLevel4 from '../../image/painLevel4.JPG';
import './Severity.scss';

// TODO make secerity-card instead of using 5 li in here
// TODO [{"name:":"No Pain", "value": 0}, {"name:":"Mid Pain", "value": 1}, ...].map(ecerity-card)
const Severity = (props) => (
  <div className="SeverityWrapper">
    <h3>Select severity level:</h3>
    <h5>Withing Parasite</h5>
    <ul>
      <li tabIndex={0} onClick={() => {props.onHandleChange({"name":"severity","value":{ "name":"No Pain", "value": 0}})}}>
        <img src={painLevel0} />
      </li>
      <li tabIndex={0} onClick={() => {props.onHandleChange({"name":"severity","value":{ "name":"Mid Pain", "value": 1}})}}>
        <img src={painLevel1} />
      </li>
      <li tabIndex={0} onClick={() => {props.onHandleChange({"name":"severity","value":{ "name":"Moderate Pain", "value": 2}})}}>
        <img src={painLevel2} />
      </li>
      <li tabIndex={0} onClick={() => {props.onHandleChange({"name":"severity","value":{ "name":"Severe Pain", "value": 3}})}}>
        <img src={painLevel3} />
      </li>
      <li tabIndex={0} onClick={() => {props.onHandleChange({"name":"severity","value":{ "name":"Deadly Pain", "value": 4}})}}>
        <img src={painLevel4} />
      </li>
    </ul>
    
  </div>
);


export default Severity;
