import React, { PureComponent } from 'react';
import { getAll } from '../../services/http';
import IllnessCard from './IllnessCard/index';
import './Illnesses.scss';

class Illnesses extends PureComponent { 
  constructor(props) {
    super(props);
    this.gridItems  = [];
    this.state = {
      illnesses: {},
      isLoading: true,
      hasError: false,
    };
  }

  handleChange = (obj) => {
    this.props.onHandleChange(obj);
  }

  callBack = (data) => {
    this.setState({ illnesses: data}, () => {
      this.gridItems  = this.state.illnesses.map((illness) => {
        return IllnessCard(illness, this.handleChange);
      });
    });
    this.setState({isLoading: false});
    
  }

  componentDidMount() {
    getAll("illnesses", this.callBack);
  }


  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    if (this.state.isLoading) {
      return <h1>is loading ...</h1>;
    }
    return (
      <div className="IllnessesWrapper">
        <h4>Select an Illnes:</h4>
        <ul>
          {this.gridItems }
        </ul> 
      </div>
    );
  }
}

export default Illnesses;
