import React from 'react';
import './IllnessCard.scss';

const IllnessCard = (props, change) => (
  <li tabIndex={0} key={props.id} onClick={() => change({"name":"illness","value":{ "name":props.name, "value": props.id}})} className="IllnessCardWrapper">
    <span>{props.name}</span>
  </li>
);


export default IllnessCard;
