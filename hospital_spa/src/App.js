import React, { PureComponent } from 'react';
import logo from './logo.svg';
import './App.scss';
import Illnesses from './components/Illnesses/index';
import Hospitals from './components/Hospitals/index';
import Severity from './components/Severity/index';
import Booking from './components/Booking/index';

const initialState = {
  /* etc */
};
class App extends PureComponent { 
  constructor(props) {
    super(props);
    this.state = {
      illness: {
        name: '',
        value: 1
      },
      severity: {
        name: '',
        value: 0
      },
      hospital: {
        name: '',
        value: 1
      },
    };
  }

  handleChange = (obj) => {
    //console.log(obj.name + obj.value);
    this.setState({ [obj.name] : obj.value}, () => { console.log(this.state.severity)});
  }

  render(){
    return(
      <div className="App">
        <Illnesses onHandleChange={this.handleChange}/>
        <Hospitals onHandleChange={this.handleChange} severity={this.state.severity.value}/>
        <Severity onHandleChange={this.handleChange} />
        {/* TODO put all props in one object */}
        <Booking onHandleChange={this.handleChange} illness={this.state.illness} hospital={this.state.hospital} severity={this.state.severity}/>

    </div>
    )
  }
};

export default App;
