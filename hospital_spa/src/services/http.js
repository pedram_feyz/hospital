import axios from 'axios';
import { devConfig } from '../config';

const baesUrl = devConfig.apiUrl;

export function getAll(entity, callback){
    axios.get(`${baesUrl}${entity}`)
    .then(res => {
        callback(res.data);
    });
};

export function get(entity, id, callback){
    axios.get(`${baesUrl}${entity}/${id}`)
    .then(res => {
        callback(res.data);
    });
};

export function booking(entity, values, callback){
    console.log(values);
    const request = axios.post(`${baesUrl}${entity}`, values);
    request.then(
        function(res) {
            const data = res.data;
            callback(data);
        },
        function(err) {
            // todo show a message to the user
        }
    );
}